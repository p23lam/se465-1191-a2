# credit Eli Bendersky
# http://eli.thegreenplace.net/2010/01/28/generating-random-sentences-from-a-context-free-grammar

from collections import defaultdict
import random
import string
import os.path

word_file = "/usr/share/dict/words"
if os.path.isfile(word_file):
    WORDS = open(word_file).read().splitlines()
else:
    WORDS = ["aardvark", "mountain", "avalanche", "crevasse", "bergschrund", "snowflake"]

class CFG(object):
    def __init__(self):
        self.prod = defaultdict(list)

    def add_prod(self, lhs, rhs):
        """ Add production to the grammar. 'rhs' can
            be several productions separated by '|'.
            Each production is a sequence of symbols
            separated by whitespace.

            Usage:
                grammar.add_prod('NT', 'VP PP')
                grammar.add_prod('Digit', '1|2|3|4')
        """
        prods = rhs.split('|')
        for prod in prods:
            self.prod[lhs].append(tuple(prod.split(' ')))

    def gen_random_convergent(self,
                              symbol,
                              cfactor=0.25,
                              pcount=defaultdict(int)
    ):
        """ Generate a random sentence from the
        grammar, starting with the given symbol.
        
        Uses a convergent algorithm - productions
        that have already appeared in the
        derivation on each branch have a smaller
        chance to be selected.
        
        cfactor - controls how tight the
        convergence is. 0 < cfactor < 1.0
        
        pcount is used internally by the
        recursive calls to pass on the
        productions that have been used in the
        branch.
        """
        sentence = ''
        
        # The possible productions of this symbol are weighted
        # by their appearance in the branch that has led to this
        # symbol in the derivation
        #
        weights = []
        for prod in self.prod[symbol]:
            if prod in pcount:
                weights.append(cfactor ** (pcount[prod]))
            else:
                weights.append(1.0)
        rand_prod = self.prod[symbol][weighted_choice(weights)]
                
        # pcount is a single object (created in the first call to
        # this method) that's being passed around into recursive
        # calls to count how many times productions have been
        # used.
        # Before recursive calls the count is updated, and after
        # the sentence for this call is ready, it is rolled-back
        # to avoid modifying the parent's pcount.
        #
        pcount[rand_prod] += 1

        for sym in rand_prod:
            if sym.endswith('?'):
                if (random.random() < cfactor):
                    continue
                else:
                    sym = sym[:-1]
            # for non-terminals, recurse
            if sym in self.prod:
                sentence += self.gen_random_convergent(
                    sym,
                    cfactor=cfactor,
                    pcount=pcount)
            elif sym in special_productions:
                sentence += generate_special_production(sym)
            else:
                sentence += sym

        # backtracking: clear the modification to pcount
        pcount[rand_prod] -= 1
        return sentence

special_productions = ['STRING20', 'WORDS', 'YEAR', 'DIGIT', 'SPACE']
def generate_string(size=6, chars=string.ascii_letters + string.digits):
    return ''.join(random.choice(chars) for _ in range(size))

def generate_special_production(sym):
    stringstring = 'STRING'
    if sym.startswith(stringstring):
        n = sym[len(stringstring):]
        return generate_string(int(n))
    if sym == 'WORDS':
        return random.choice(WORDS)
    if sym == 'YEAR':
        return "{:04d}".format(random.randint(1970,2099))
    if sym == 'DIGIT':
        return "{:d}".format(random.randint(0,9))
    if sym == 'SPACE':
        return ' '
    
def weighted_choice(weights):
    rnd = random.random() * sum(weights)
    for i, w in enumerate(weights):
        rnd -= w
        if rnd < 0:
            return i

cfg = CFG()
cfg.add_prod('ICS', 'BEGIN:VCALENDAR \n VERSION:2.0 \n PROD_ID EVENTS END:VCALENDAR \n')
cfg.add_prod('PROD_ID', 'PRODID:-//plam//plam//EN\n')
cfg.add_prod('EVENTS', 'EVENT EVENTS?')
cfg.add_prod('EVENT', 'fill_this_in SPACE WORDS \n');

print cfg.gen_random_convergent('ICS')
